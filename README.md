# T01 - JS BASIC

This repo contains javascript exercises solved during the lecture of 01/03/2024



## Content

The project consists in:

- A page with data and questions, in which the javascript code should be written (exercises.html)
- A page with a possible solution to the exercises that will print the results to the web console (solutions.html)